To build the go app, run the below command
```
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
```

to build the docker image, run the below command
```
docker build -t <tag> .
```

