package main

import (
	"fmt"
        "net/http"

        "github.com/prometheus/client_golang/prometheus/promhttp"
)

func Helloserver(w http.ResponseWriter, r *http.Request){
	fmt.Println("handling request: ", r)
	i := 1000
	sum := 0
	for j := 0; j < i; j++{
		sum += j
	}
	fmt.Fprintf(w, "hello sum %d", sum)
}
func main() {
	fmt.Println("starting server")
        http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/hello", Helloserver)
        http.ListenAndServe(":8086", nil)
}

