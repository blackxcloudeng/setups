package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// InterfaceNetStats stores network statistics about a Docker network interface
type InterfaceNetStats struct {
	NetworkName string
	BytesSent   uint64
	BytesRcvd   uint64
	PacketsSent uint64
	PacketsRcvd uint64
}

// ContainerNetStats stores network statistics about a Docker container per interface
type ContainerNetStats []*InterfaceNetStats

// readLines reads contents from a file and splits them by new lines.
func readLines(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return []string{""}, err
	}
	defer f.Close()

	var ret []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		ret = append(ret, scanner.Text())
	}
	return ret, scanner.Err()
}

// hostProc returns the location of a host's procfs. This can and will be
// overridden when running inside a container.
func hostProc(combineWith ...string) string {
	parts := append([]string{"/proc"}, combineWith...)
	return filepath.Join(parts...)
}

// pathExists returns a boolean indicating if the given path exists on the file system.
func pathExists(filename string) bool {
	if _, err := os.Stat(filename); err == nil {
		return true
	}
	return false
}

// CollectNetworkStats retrieves the network statistics for a given pid.
// The networks map allows to optionnaly map interface name to user-friendly
// network names. If not found in the map, the interface name is used.
func CollectNetworkStats(pid int, networks map[string]string) (ContainerNetStats, error) {
	netStats := ContainerNetStats{}

	procNetFile := hostProc("net", "dev")
	if !pathExists(procNetFile) {
		fmt.Printf("Unable to read %s for pid %d", procNetFile, pid)
		return netStats, nil
	}
	lines, err := readLines(procNetFile)
	if err != nil {
		fmt.Printf("Unable to read %s for pid %d", procNetFile, pid)
		return netStats, nil
	}
	if len(lines) < 2 {
		return nil, fmt.Errorf("invalid format for %s", procNetFile)
	}

	// Format:
	//
	// Inter-|   Receive                                                |  Transmit
	// face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
	// eth0:    1296      16    0    0    0     0          0         0        0       0    0    0    0     0       0          0
	// lo:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
	//
	for _, line := range lines[2:] {
		fields := strings.Fields(line)
		if len(fields) < 11 {
			continue
		}
		iface := fields[0][:len(fields[0])-1]

		var stat *InterfaceNetStats

		if nw, ok := networks[iface]; ok {
			stat = &InterfaceNetStats{NetworkName: nw}
		} else if iface == "lo" {
			continue // Ignore loopback
		} else {
			stat = &InterfaceNetStats{NetworkName: iface}
		}

		rcvd, _ := strconv.Atoi(fields[1])
		stat.BytesRcvd = uint64(rcvd)
		pktRcvd, _ := strconv.Atoi(fields[2])
		stat.PacketsRcvd = uint64(pktRcvd)
		sent, _ := strconv.Atoi(fields[9])
		stat.BytesSent = uint64(sent)
		pktSent, _ := strconv.Atoi(fields[10])
		stat.PacketsSent = uint64(pktSent)

		netStats = append(netStats, stat)
	}
	return netStats, nil
}

func main() {
	pid := 2253
	/*
	networks := map[string]string{
		"eth0": "bridge",
	}
	*/
	for {
		stats, err := CollectNetworkStats(pid, map[string]string{})
		if err != nil {
			fmt.Println(err)
		}
		//fmt.Printf("stats %+v",stats)
		for _, x := range stats{
			fmt.Printf("Networkname %s,Bytesrx %d, bytes send %d, packetsrxd %d, packetssent %d\n", x.NetworkName, x.BytesRcvd, x.BytesSent, x.PacketsRcvd,x.PacketsSent)
		}
		time.Sleep(60 * time.Second)

	}
}

