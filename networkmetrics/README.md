# To build from docker file directly
docker build -t godocker -f Dockerfile .
docker run -it godocker


#Build first and then copy binary to docker
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./network-analyzer .
docker build -t network-analyzer -f Dockerfile.scratch .
docker run -it network-analyzer

