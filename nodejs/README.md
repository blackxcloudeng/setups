To build the docker image, run the below command
```
docker build -t blackxcloudeng/node-hello-world:latest .
docker tag blackxcloudeng/node-hello-world:latest blackxcloudeng/node-hello-world:latest
docker push blackxcloudeng/node-hello-world:latest
```

To test the docker image, run the below command
```
docker run -d --rm --name node-hello-world -p 19997:8080 blackxcloudeng/node-hello-world:latest
curl -v localhost:19997/metrics
```
