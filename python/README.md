To build the docker image, run the below command
```
docker build -t blackxcloudeng/python-hello-world:latest .
docker tag blackxcloudeng/python-hello-world:latest blackxcloudeng/python-hello-world:latest
docker push blackxcloudeng/python-hello-world:latest
```

To test the docker image, run the below command
```
docker run -d --rm --name python-hello-world -p 19999:8080 -p 19998:80 blackxcloudeng/python-hello-world:latest
curl -v localhost:19998
curl -v localhost:19999/metrics
```
